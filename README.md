# Markdown Tufte-Handout

LaTeX Template for Pandoc to quickly generate notes with Markdown. It extends
the default LaTeX template from Pandoc to use the `tufte-handout` documentclass.

## Usage

Simply clone the repository and link the template.

```bash
ln -s /path/to/tufte-pandoc-tex/tufte.latex ~/.pandoc/templates/
```

To compile a document run:

```bash
pandoc example.md -o example.pdf --template tufte --listings
```

or use the provided VSCode build task.
