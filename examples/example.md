---
lang: en-US
title: Example Document
author: Benjamin Faller
abstract: 'Just an example document to showcase the features of the Markdown to
    \LaTeX{} compilation with Pandoc'
---

# Getting Started

Here we have a simple list of steps:

1. do something
2. be creative
3. finish the work.

# Features

## Math

We can write inline formulas ($c^2 = a^2 + b^2$) or via the align environment.
References are also possible which can be seen with
equation&nbsp;\eqref{eq:example}.

\begin{align}
  u &= \sum^n_{j=1} \frac{t_{E_{\text{max}, j}}}
    {\min t_{D_{max}, j}, t_{P_{\text{min}, j}}}
    \le n \left(2^{\frac{1}{n}}-1\right)
    \label{eq:example}
\end{align}

Units are supported with `siunitx` like \SI{100}{\kilo\metre\per\hour}.

## Code

```python
def main():
    print("Hello World") # Output "Hello World"

if __name__ == "__main__":
    main()
```

## Tables

| Col1        | Alignment     | Value  |
| ----------- |:-------------:| ------:|
| col 3 is    | right-aligned |  1     |
| col 2 is    | centered      |  2     |
| col 1 is    | normal        |  3     |

## Quotes

Quotations can be shown with

```text
> quote
```

> It is love that will save our world and our civilization, love even for
> enemies. --- Martin Luther King, Jr.

## Sidenotes

This sentence[^1]

[^1]: ends here.

## Figures

![Close-Up Photography of Leaves With Droplets --- Photo by sohail na from
Pexels](./img/leaves.jpg)

Figures on the right side have to be written in \LaTeX{} since we can't do this
with Markdown.

\begin{marginfigure}
  \includegraphics[width=\linewidth]{./img/leaves.jpg}
  \caption{Close-Up Photography of Leaves With Droplets --- Photo by sohail na
    from Pexels}
\end{marginfigure}
